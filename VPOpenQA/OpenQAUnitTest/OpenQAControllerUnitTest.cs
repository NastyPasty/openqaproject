﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using GetData = VPOpenQA.Controllers.OpenQAController;
namespace OpenQAUnitTest
{
	[TestClass]
	public class OpenQAControllerUnitTest : GetData
	{
		[TestMethod]
		public void GetCities()
		{ 
			var cityData = GetCitiesByCountryRequestResult("GB");
			
			Assert.IsNotNull(cityData);
		
		}
	}
}
