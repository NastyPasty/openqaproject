﻿using Newtonsoft.Json;
using System.IO;
using System.Net;
using VPOpenQA.Models;

namespace VPOpenQA.Controllers
{
	public class OpenQAController
	{
        private static HttpWebRequest GetWebRequest(string URL, string extension)
        {
            HttpWebRequest webRequest = WebRequest.Create(URL + extension) as HttpWebRequest;
            webRequest.Method = "GET";
            webRequest.UserAgent = "Anything";
            webRequest.ServicePoint.Expect100Continue = false;

            return webRequest;
        }

        private static object getCitiesByCountryRequest(string URL, string extension)
        {
            CitiesModel response = null;
            var webRequest = GetWebRequest(URL, extension);
            using (StreamReader responseReader = new StreamReader(webRequest.GetResponse().GetResponseStream()))
            {
                var reader = responseReader.ReadToEnd();
               
                response = JsonConvert.DeserializeObject<CitiesModel>(reader);

            }
            return response;
        }
        private static object getMeasurementByCityRequest(string URL, string extension)
        {
            MeasurementModel response = null;
            var webRequest = GetWebRequest(URL, extension);
            using (StreamReader responseReader = new StreamReader(webRequest.GetResponse().GetResponseStream()))
            {
                string reader = responseReader.ReadToEnd();
                response = JsonConvert.DeserializeObject<MeasurementModel>(reader);

            }
            return response;
        }
   

        public static object GetCitiesByCountryResult(string country)
        {
            var URL = "https://api.openaq.org/v1/cities?country=";
            var countryExtension = country;
            var jsonobj = getCitiesByCountryRequest(URL, countryExtension);
            return jsonobj;

        }

        public static object GetMeasurementByCityResult(string city)
        {
            var URL = "https://api.openaq.org/v1/measurements?city=";
            var countryExtension = city;
            var jsonobj = getMeasurementByCityRequest(URL, countryExtension);
            return jsonobj;
        }

    }
}