﻿using System.Web.Mvc;
using Variables = VPOpenQA.Models.VariableModel;
using Cities = VPOpenQA.Models.CitiesModel;
using getData = VPOpenQA.Controllers.OpenQAController;

namespace VPOpenQA.Controllers
{
	public class HomeController : Controller
	{
		public ActionResult Index(string value)
		{
            
            return View();
		}

        [HttpGet]
        public ActionResult GetCityDetails(string country)
        {
              
            var Cities = getData.GetCitiesByCountryResult(country);
            
            return View("Index" ,Cities);
        }

        [HttpPost]
        public ActionResult GetCityBYCountry(Variables Item1)
        {
            if (ModelState.IsValid)
            {
                GetCityDetails(Item1.Country);
            }

            return View("Index");
        }
    }
}
