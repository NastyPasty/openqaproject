﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VPOpenQA.Models
{
	public class CitiesModel
	{
		[JsonProperty("results")]
		public List<Result> Results { get; set; }

		public class Result   
		{
			public string country { get; set; }
			public string name { get; set; }
			public string city { get; set; }
			public string location { get; set; }
			public string count { get; set; }
		}
		
		
	}
	
}