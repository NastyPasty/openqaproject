﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VPOpenQA.Models
{
	public class MeasurementModel
	{
		public int value { get; set; }
		public string unit { get; set; }
		public string country { get; set; }
		public string city { get; set; }
	}
}