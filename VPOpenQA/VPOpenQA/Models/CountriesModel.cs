﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VPOpenQA.Models
{
	public class CountriesModel
	{
		public string code { get; set; }
		public string name { get; set; }
	}
}